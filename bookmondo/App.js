import React, { useEffect, useState } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SplashScreen from 'react-native-splash-screen';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { ThemeProvider } from 'react-native-elements';
import { LoadingContext } from '@contexts';
import { AuthNavigator } from '@navigators';
import MainTheme from '@themes/main';

const App = () => {
	useEffect(() => {
		SplashScreen.hide();
		GoogleSignin.configure({
			webClientId:
				'820096837528-6q6c4jje2rnuhhi5vrq5sbl3ff3f4iej.apps.googleusercontent.com',
		});
	}, []);

	return (
		<SafeAreaProvider>
			<ThemeProvider theme={MainTheme}>
				<AuthNavigator />
			</ThemeProvider>
		</SafeAreaProvider>
	);
};

export default App;
