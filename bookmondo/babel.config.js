module.exports = {
	presets: ['module:metro-react-native-babel-preset'],
	plugins: [
		[
			'module-resolver',
			{
				root: ['./src'],
				extensions: ['.jsx', '.js', '.json'],
				alias: {
					'@components': './src/components',
					'@contexts': './src/contexts',
					'@hooks': './src/hooks',
					'@navigators': './src/navigators',
					'@screens': './src/screens',
					'@utils': './src/utils',
					'@assets': './src/assets',
					'@styles': './src/styles',
					'@themes': './src/themes',
				},
			},
			'react-native-reanimated/plugin',
		],
	],
};
