import * as React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Colors } from '@styles';
import {BooksScreen, SettingsScreen} from '@screens';
import BooksNavigator from './BooksNavigator';

function HomeScreen() {
	return (
		<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
			<Text>Home!</Text>
		</View>
	);
}
const Tab = createBottomTabNavigator();

const MainNavigator = () => {
	return (
		<Tab.Navigator
			tabBarOptions={{
				activeTintColor: Colors.SECONDARY_PINK,
				inactiveTintColor: Colors.PRIMARY_BLUE,
			}}
		>
			<Tab.Screen
				name="Books"
				component={BooksNavigator}
				options={{
					tabBarIcon: ({ color }) => (
						<Entypo name="book" size={24} color={color} />
					),
				}}
			/>
			<Tab.Screen
				name="Shelves"
				component={SettingsScreen}
				options={{
					tabBarIcon: ({ color }) => (
						<MaterialCommunityIcons name="bookshelf" size={24} color={color} />
					),
				}}
			/>
			<Tab.Screen
				name="Settings"
				component={SettingsScreen}
				options={{
					tabBarIcon: ({ color }) => (
						<Entypo name="cog" size={24} color={color} />
					),
				}}
			/>
		</Tab.Navigator>
	);
};

export default MainNavigator;
