import React, { useState, useEffect } from 'react';
import auth from '@react-native-firebase/auth';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SignInScreen, SignUpScreen } from '@screens';
import MainNavigator from './MainNavigator';

const Stack = createStackNavigator();

const AuthNavigator = () => {
	const [initializing, setInitializing] = useState(true);
	const [user, setUser] = useState();

	function onAuthStateChanged(user) {
		setUser(user);
		if (initializing) setInitializing(false);
	}

	useEffect(() => {
		const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
		return subscriber; // unsubscribe on unmount
	}, []);

	return (
		<NavigationContainer>
			<Stack.Navigator screenOptions={{ headerShown: false }}>
				{user ? (
					<>
						<Stack.Screen name="Main" component={MainNavigator} />
					</>
				) : (
					<>
						<Stack.Screen name="SignUp" component={SignUpScreen} />
						<Stack.Screen name="SignIn" component={SignInScreen} />
					</>
				)}
			</Stack.Navigator>
		</NavigationContainer>
	);
};

export default AuthNavigator;
