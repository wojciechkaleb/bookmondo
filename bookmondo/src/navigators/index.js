export {default as AuthNavigator} from './AuthNavigator';
export {default as MainNavigator} from './MainNavigator';
export {default as BooksNavigator} from './BooksNavigator';