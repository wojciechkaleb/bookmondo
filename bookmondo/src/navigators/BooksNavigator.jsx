import * as React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { AllBooksScreen } from '@screens';
import { CreateBookForm, UpdateBookForm } from '@components';

const Stack = createStackNavigator();

function HomeScreen() {
	return (
		<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
			<Text>Home Screen</Text>
		</View>
	);
}

const BooksNavigator = () => (
	<Stack.Navigator>
		<Stack.Screen
			name="AllBooksScreen"
			component={AllBooksScreen}
			options={{ headerShown: false }}
		/>
		<Stack.Screen
			name="UpdateBook"
			component={UpdateBookForm}
			options={({ route }) => ({ title: route.params.title })}
		/>
		<Stack.Screen name="CreateBook" component={CreateBookForm} />
	</Stack.Navigator>
);

export default BooksNavigator;
