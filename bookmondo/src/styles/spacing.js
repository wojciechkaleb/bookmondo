import {
	heightPercentageToDP as hp,
	widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export const vertical = {
	margin: {
		largest: hp(10),
		large: hp(7.5),
		base: hp(5),
		small: hp(2.5),
		smallest: hp(1.5),
	},
	padding: {
		largest: hp(10),
		large: hp(7.5),
		base: hp(5),
		small: hp(2.5),
		smallest: hp(1.5),
	},
};
