export const GOOGLE_RED = "#DB4437"
export const FACEBOOK_BLUE = "#4267B2";

// Theme Colors
export const PRIMARY_BLUE = "#7175bf";
export const SECONDARY_PINK = "#e8299a"
export const WHITE = "#e8e9fa";
export const BLACK = "#222226";

export const GREY0 = "#202020";
export const GREY1 = "#282828";
export const GREY2 = "#303030";
export const GREY3 = "#383838";
export const GREY4 = "#404040";
export const GREY5 = "#484848";

export const SUCCESS = "#14914f";
export const ERROR = "#a11238";
export const WARNING = "#d9a536";