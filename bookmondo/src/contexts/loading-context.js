import React, {useContext} from 'react';
const initState = {
	isLoading: false,
	setIsLoading: () => {},
};

export default React.createContext(initState);
