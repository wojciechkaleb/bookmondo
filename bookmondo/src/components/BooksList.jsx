import React from 'react';
import { FlatList, StyleSheet} from 'react-native';
import BookListItem from './BookListItem';
import {Spacing} from '@styles'
const BooksList = ({ books }) => {
	return (
		<FlatList
			style={styles.container}
			data={books}
			renderItem={({ item }) => (
				<BookListItem item={item} />
			)}
		/>
	);
};

export default BooksList;

const styles = StyleSheet.create({
	container: {
		marginBottom: Spacing.vertical.margin.base
	}
})