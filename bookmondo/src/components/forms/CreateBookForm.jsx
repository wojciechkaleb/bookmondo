import React, { useState } from 'react';
import { KeyboardAvoidingView, StyleSheet } from 'react-native';
import { Input, CheckBox, Button } from 'react-native-elements';
import {
	heightPercentageToDP as hp,
	widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import firestore from '@react-native-firebase/firestore';
import { Spacing, Colors } from '@styles';
import { useMe } from '@hooks/use-me';

const CreateBookForm = ({ navigation }) => {
	const [me] = useMe();
	const [form, setForm] = useState({
		title: '',
		author: '',
		max_pages: '',
		show_as_percentage: false,
		current_page: 0,
	});

	const ref = firestore().collection('books');

	const addBook = () => {
		const timestamp = firestore.FieldValue.serverTimestamp();
		ref
			.add({
				...form,
				max_pages: parseInt(form.max_pages),
				user: me.uid,
				created: timestamp,
				updated: timestamp,
			})
			.then(() => {
				navigation.navigate('AllBooksScreen');
			});
	};

	return (
		<KeyboardAvoidingView style={styles.container}>
			<Input
				onChangeText={(text) => setForm((state) => ({ ...state, title: text }))}
				placeholder="Title"
				value={form.title}
				containerStyle={styles.inputContainer}
				placeholderTextColor={Colors.SECONDARY_PINK}
				inputStyle={styles.input}
				inputContainerStyle={styles.inputContainerOuter}
			/>
			<Input
				onChangeText={(text) =>
					setForm((state) => ({ ...state, author: text }))
				}
				placeholder="Author"
				value={form.author}
				containerStyle={styles.inputContainer}
				placeholderTextColor={Colors.SECONDARY_PINK}
				inputStyle={styles.input}
				inputContainerStyle={styles.inputContainerOuter}
			/>
			<Input
				onChangeText={(text) =>
					setForm((state) => ({ ...state, max_pages: text }))
				}
				keyboardType="numeric"
				placeholder="Pages"
				value={form.max_pages.toString()}
				containerStyle={styles.inputContainer}
				placeholderTextColor={Colors.SECONDARY_PINK}
				inputStyle={styles.input}
				inputContainerStyle={styles.inputContainerOuter}
			/>
			<CheckBox
				title="Show progress as percentage?"
				containerStyle={{backgroundColor: "transparent"}}
				checked={form.show_as_percentage}
				onPress={() =>
					setForm((state) => ({
						...state,
						show_as_percentage: !state.show_as_percentage,
					}))
				}
			/>
			<Button
				onPress={() => addBook()}
				buttonStyle={styles.button}
				title="Submit"
			/>
		</KeyboardAvoidingView>
	);
};

const styles = StyleSheet.create({
	container: {
		marginTop: Spacing.vertical.margin.small,
		alignItems: 'center',
	},
	inputContainer: {
		width: wp(90),
	},
	input: {
		color: Colors.SECONDARY_PINK,
	},
	label: {
		color: Colors.SECONDARY_PINK,
	},
	button: {
		backgroundColor: Colors.SECONDARY_PINK,
		width: wp(80),
		marginTop: Spacing.vertical.margin.base,
	},
	inputContainerOuter: {
		borderBottomColor: Colors.SECONDARY_PINK,
	},
});

export default CreateBookForm;
