import React, { useState, useContext } from 'react';
import auth from '@react-native-firebase/auth';
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { KeyboardAvoidingView as View, StyleSheet } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import { Colors, Spacing } from '@styles';
import { LoadingContext } from '@contexts';

const LoginForm = () => {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const {isLoading, setIsLoading} = useContext(LoadingContext);

	const signIn = () => {
		setIsLoading(true);
		auth()
			.signInWithEmailAndPassword(email, password)
			.then(() => console.log('User created successfuly'))
			.catch((err) => setErrors([err]))
			.finally(() => setIsLoading(false));
	};
	return (
		<View behavior="padding" enabled style={styles.container}>
			<Input
				onChangeText={(text) => setEmail(text)}
				placeholder="Enter your email"
				autoCompleteType="email"
				autoCapitalize="none"
				value={email}
				containerStyle={styles.inputContainer}
				placeholderTextColor={Colors.SECONDARY_PINK}
				inputStyle={styles.input}
				inputContainerStyle={styles.inputContainerOuter}
				leftIcon={
					<Icon
						name="email"
						size={24}
						color={Colors.SECONDARY_PINK}
						type="material-community"
					/>
				}
			/>
			<Input
				autoCompleteType="password"
				secureTextEntry
				onChangeText={(text) => setPassword(text)}
				value={password}
				placeholder="Enter your password"
				containerStyle={styles.inputContainer}
				placeholderTextColor={Colors.SECONDARY_PINK}
				inputStyle={styles.input}
				inputContainerStyle={styles.inputContainerOuter}
				leftIcon={
					<Icon
						name="locked"
						size={24}
						color={Colors.SECONDARY_PINK}
						type="fontisto"
					/>
				}
			/>
			<Button onPress={signIn} title="Sign in" />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		marginTop: Spacing.vertical.margin.small,
	},
	inputContainer: {
		width: wp(80),
	},
	input: {
		color: Colors.SECONDARY_PINK,
	},
	label: {
		color: Colors.SECONDARY_PINK,
	},
});

export default LoginForm;