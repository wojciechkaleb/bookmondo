import React, { useState, useEffect } from 'react';
import { KeyboardAvoidingView, StyleSheet, Animated, Text } from 'react-native';
import { Input, CheckBox, Button } from 'react-native-elements';
import {
	heightPercentageToDP as hp,
	widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Slider from '@react-native-community/slider';
import firestore from '@react-native-firebase/firestore';
import { Spacing, Colors } from '@styles';
import { useMe } from '@hooks/use-me';

const UpdateBookForm = ({ navigation, route }) => {
	const id = route.params.id;

	const [me] = useMe();
	const [form, setForm] = useState({
		title: '',
		author: '',
		max_pages: '0',
		show_as_percentage: false,
		current_page: 0,
	});

	useEffect(() => {
		const subscriber = firestore()
			.collection('books')
			.doc(id)
			.get()
			.then((values) => {
				setForm(values.data());
			})
			.catch((err) => {
				console.error(err);
			});
	}, []);

	const ref = firestore().collection('books').doc(id);

	const addBook = () => {
		const timestamp = firestore.FieldValue.serverTimestamp();
		ref
			.set({
				...form,
				max_pages: parseInt(form.max_pages),
				user: me.uid,
				created: timestamp,
				updated: timestamp,
			})
			.then(() => {
				navigation.navigate('AllBooksScreen');
			});
	};

	return (
		<KeyboardAvoidingView style={styles.container}>
			<Input
				onChangeText={(text) => setForm((state) => ({ ...state, title: text }))}
				placeholder="Title"
				value={form.title}
				containerStyle={styles.inputContainer}
				placeholderTextColor={Colors.SECONDARY_PINK}
				inputStyle={styles.input}
				inputContainerStyle={styles.inputContainerOuter}
			/>
			<Input
				onChangeText={(text) =>
					setForm((state) => ({ ...state, author: text }))
				}
				placeholder="Author"
				value={form.author}
				containerStyle={styles.inputContainer}
				placeholderTextColor={Colors.SECONDARY_PINK}
				inputStyle={styles.input}
				inputContainerStyle={styles.inputContainerOuter}
			/>
			{/* <View
				style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}
			>
				<Slider
					value={5}
					onValueChange={(value) =>
						setState((prevState) => ({ ...prevState, current_page: value }))
					}
					thumbStyle={{ height: 40, width: 40, backgroundColor: 'transparent' }}
					thumbProps={{
						Component: Animated.Image,
						source: {
							uri:
								'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
						},
					}}
				/>
				<Text>Value: </Text>
			</View>
			; */}
			<Slider
				style={{ width: wp(80), height: 40 }}
				minimumValue={0}
				maximumValue={parseInt(form.max_pages)}
				thumbTintColor={Colors.SECONDARY_PINK}
				step={20}
				value={form.current_page}
				onValueChange={(value) =>
					setForm((prevState) => ({
						...form,
						current_page: value,
					}))
				}
				minimumTrackTintColor={Colors.GREY5}
				maximumTrackTintColor={Colors.GREY0}
			/>
			{form.show_as_percentage ? (
				<Text>{Math.round((form.current_page / form.max_pages) * 100)}%</Text>
			) : (
				<Text>
					{form.current_page} / {form.max_pages}
				</Text>
			)}
			<Text>{form.max_pagddes}</Text>
			<CheckBox
				title="Show progress as percentage?"
				containerStyle={{
					backgroundColor: 'transparent',
					borderColor: 'transparent',
				}}
				checked={form.show_as_percentage}
				onPress={() =>
					setForm((state) => ({
						...state,
						show_as_percentage: !state.show_as_percentage,
					}))
				}
			/>
			<Button
				onPress={() => addBook()}
				buttonStyle={styles.button}
				title="Submit"
			/>
		</KeyboardAvoidingView>
	);
};

const styles = StyleSheet.create({
	container: {
		marginTop: Spacing.vertical.margin.small,
		alignItems: 'center',
	},
	inputContainer: {
		width: wp(90),
	},
	input: {
		color: Colors.SECONDARY_PINK,
	},
	label: {
		color: Colors.SECONDARY_PINK,
	},
	button: {
		backgroundColor: Colors.SECONDARY_PINK,
		width: wp(80),
		marginTop: Spacing.vertical.margin.base,
	},
	inputContainerOuter: {
		borderBottomColor: Colors.SECONDARY_PINK,
	},
});

export default UpdateBookForm;
