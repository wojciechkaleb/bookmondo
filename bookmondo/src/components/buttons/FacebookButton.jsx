import React from 'react';
import auth from '@react-native-firebase/auth';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Spacing, Colors } from '@styles';

const FacebookButton = ({ title, style }) => {
	const signIn = async () => {
		try {
			const result = await LoginManager.logInWithPermissions([
				'public_profile',
				'email',
			]);

			if (result.isCancelled) {
				throw 'User cancelled the login process';
			}

			const data = await AccessToken.getCurrentAccessToken();

			if (!data) {
				throw 'Something went wrong obtaining access token';
			}

			const facebookCredential = auth.FacebookAuthProvider.credential(
				data.accessToken
			);
			return auth().signInWithCredential(facebookCredential);
		} catch (error) {
			console.error(error);
		}
	};
	return (
		<Button
			icon={{
				name: 'facebook',
				size: wp(7.5),
				color: 'white',
				brand: true,
				type: 'font-awesome-5',
				style: styles.icon,
			}}
			buttonStyle={[styles.button, style]}
			title={title}
			onPress={() => {
				signIn().then(() => {
					onSuccess();
				});
			}}
		/>
	);
};

const styles = StyleSheet.create({
	button: {
		backgroundColor: Colors.FACEBOOK_BLUE,
		width: wp(80),
	},
	icon: {
		marginRight: Spacing.vertical.margin.small,
	},
});
export default FacebookButton;
