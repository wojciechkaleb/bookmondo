import React from 'react';
import auth from '@react-native-firebase/auth';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Spacing, Colors } from '@styles';

const GoogleButton = ({ title, style }) => {
	const signIn = async () => {
		try {
			const { idToken } = await GoogleSignin.signIn();
			const googleCredential = auth.GoogleAuthProvider.credential(idToken);
			return auth().signInWithCredential(googleCredential);
		} catch (error) {
			console.log(error);
			console.error(error);
		}
	};
	return (
		<Button
			icon={{
				name: 'google',
				size: wp(7.5),
				color: 'white',
				brand: true,
				type: 'font-awesome-5',
				style: styles.icon,
			}}
			buttonStyle={[styles.button, style]}
			title={title}
			onPress={signIn}
		/>
	);
};

const styles = StyleSheet.create({
	button: {
		backgroundColor: Colors.GOOGLE_RED,
		width: wp(80),
	},
	icon: {
		marginRight: Spacing.vertical.margin.small,
	},
	inputContainerOuter: {
		borderBottomColor: Colors.SECONDARY_PINK
	}
});
export default GoogleButton;
