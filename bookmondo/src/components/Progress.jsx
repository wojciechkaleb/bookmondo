import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {Colors} from '@styles';
const ProgressBar = ({completed}) => {
	return (
		<View style={styles.containerStyles}>
			<View style={[styles.fillerStyles, { width: `${completed}%`}]}>
				<Text style={styles.labelStyles}>{`${completed}%`}</Text>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	containerStyles: {
		height: 20,
		// width: '100%',
		backgroundColor: '#e0e0de',
		borderRadius: 50,
		margin: 50,
	},
	fillerStyles: {
		height: '100%',
		backgroundColor: Colors.SECONDARY_PINK,
		textAlign: 'right',
	},
	labelStyles: {
		padding: 5,
		color: 'white',
		fontWeight: 'bold',
	},
});
export default ProgressBar;
