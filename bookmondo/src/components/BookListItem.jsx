import React, { createRef } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { ListItem, Button } from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Slider from '@react-native-community/slider';
import ActionSheet from 'react-native-actions-sheet';
import { useNavigation } from '@react-navigation/native';
import { useMe } from '@hooks/use-me';
import { Spacing, Colors } from '@styles';

const actionSheetRef = createRef();

const BookListItem = ({ item }) => {
	let actionSheet;

	const navigation = useNavigation();
	const [me] = useMe();

	const deleteBook = () =>
		firestore().collection('books').doc(item.key).delete();

	const current = Math.floor((item.current_page / item.max_pages) * 100);

	return (
		<ListItem
			onLongPress={() => {
				actionSheetRef.current?.setModalVisible();
			}}
			onPress={() => {
				navigation.navigate('UpdateBook', { id: item.key, title: item.title });
			}}
			style={{ paddingRight: Spacing.vertical.padding.small }}
			bottomDivider
		>
			<View style={styles.listItemView}>
				<ListItem.Title>{item.title}</ListItem.Title>
				<ListItem.Subtitle style={styles.listItemAuthor}>
					{item.author}
				</ListItem.Subtitle>
				<Slider
					style={{ width: wp(80), height: 40 }}
					minimumValue={0}
					maximumValue={item.max_pages}
					thumbTintColor={Colors.SECONDARY_PINK}
					step={20}
					value={parseInt(item.current_page)}
					minimumTrackTintColor={Colors.GREY5}
					maximumTrackTintColor={Colors.GREY0}
					disabled
				/>
				{item.show_as_percentage ? (
					<Text style={styles.progressText}>{current}%</Text>
				) : (
					<Text style={styles.progressText}>
						{item.current_page} / {item.max_pages}
					</Text>
				)}
			</View>
			<ListItem.Chevron />
			<ActionSheet ref={actionSheetRef}>
				<View style={styles.actionSheet}>
					<Button
						buttonStyle={styles.deleteButton}
						title={'Delete?'}
						onPress={() => deleteBook()}
					></Button>
				</View>
			</ActionSheet>
		</ListItem>
	);
};

styles = StyleSheet.create({
	actionSheet: {
		paddingVertical: Spacing.vertical.padding.base,
		alignItems: 'center',
	},
	deleteButton: {
		backgroundColor: Colors.ERROR,
		marginTop: Spacing.vertical.margin.large,
		paddingVertical: Spacing.vertical.padding.small,
		width: wp(80),
	},
	listItemView: {
		paddingLeft: 10,
		paddingTop: 5,
		width: wp(85),
		height: hp(15),
	},
	progressText: {
		color: Colors.SECONDARY_PINK,
		alignSelf: 'flex-end',
	},
	listItemAuthor: {
		marginTop: Spacing.vertical.margin.smallest,
	},
});

export default BookListItem;
