import React from 'react';
import { Image } from 'react-native';
const FullLogo = (props) => (
	<Image source={require('@assets/img/bookmondo_logo_full.png')} {...props} />
);

export default FullLogo;
