import React from 'react';
import { View, StyleSheet, ColorPropType } from 'react-native';
import { Button, Text } from 'react-native-elements';
import auth from '@react-native-firebase/auth';
import { Gravatar, GravatarApi } from 'react-native-gravatar';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Spacing, Colors } from '@styles';

import { useMe } from '@hooks/use-me';

const SettingsScreen = () => {
	const [me] = useMe();

	return (
		<View style={styles.container}>
			<Gravatar
				options={{
					email: me.email,
					parameters: { size: '200', d: 'mm' },
					secure: true,
				}}
				style={styles.roundedProfileImage}
			/>
			<Text h2>{me.displayName}</Text>
			<Text h4 style={styles.email}>
				{me.email}
			</Text>
			<Button
				buttonStyle={styles.logoutButton}
				title="Sign Out"
				onPress={() => auth().signOut()}
			/>
		</View>
	);
};
const styles = StyleSheet.create({
	container: {
		paddingTop: Spacing.vertical.padding.large,
		alignItems: 'center',
	},
	roundedProfileImage: {
		width: 100,
		height: 100,
		borderWidth: 3,
		borderColor: 'white',
		borderRadius: 50,
		marginBottom: Spacing.vertical.margin.base,
	},
	email: {
		marginTop: Spacing.vertical.margin.small,
		color: Colors.GREY5,
		opacity: 0.5,
	},
	logoutButton: {
		backgroundColor: Colors.ERROR,
		marginTop: Spacing.vertical.margin.large,
		width: wp(80)
	},
});
export default SettingsScreen;
