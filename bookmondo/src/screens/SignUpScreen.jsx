import React from 'react';
import { KeyboardAvoidingView, StyleSheet } from 'react-native';
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
	GoogleButton,
	FacebookButton,
	FullLogo,
	RegisterForm,
} from '@components';
import { Spacing } from '@styles';

const SignUpScreen = ({ navigation }) => {

	return (
		<KeyboardAvoidingView style={styles.container}>
			<FullLogo style={styles.logo} />
			<RegisterForm navigation={navigation} />
			<GoogleButton
				title="Sign up with Google"
				style={styles.googleButton}
			/>
			<FacebookButton
				title="Sign up with Facebook"
				style={styles.button}
			/>
		</KeyboardAvoidingView>
	);
};

const styles = StyleSheet.create({
	container: {
		alignItems: 'center',
		justifyContent: 'flex-end',
		paddingVertical: Spacing.vertical.padding.small,
	},
	button: {
		marginTop: Spacing.vertical.margin.small,
	},
	googleButton: {
		marginTop: Spacing.vertical.margin.largest,
	},
	logo: {
		width: wp(80),
		height: hp(30),
		resizeMode: 'stretch',
	},
});

export default SignUpScreen;
