import React, { useState, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import {
	heightPercentageToDP as hp,
	widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import firestore from '@react-native-firebase/firestore';
import { BookList } from '@components';
import { useMe } from '@hooks/use-me';
import { Colors } from '@styles';

const AllBooksScreen = ({ navigation }) => {
	const [me] = useMe();
	const [loading, setLoading] = useState(true);
	const [books, setBooks] = useState([]);

	useEffect(() => {
		const subscriber = firestore()
			.collection('books')
			.where('user', '==', me.uid)
			.onSnapshot((querySnapshot) => {
				const books = [];
				querySnapshot.forEach((documentSnapshot) => {
					books.push({
						...documentSnapshot.data(),
						key: documentSnapshot.id
					});
				});
				setBooks(books);
				setLoading(false);
			});
		return () => subscriber();
	}, []);

	return (
		<View style={styles.container}>
			{loading ? (
				<ActivityIndicator animating color={Colors.SECONDARY_PINK} />
			) : (
				<BookList books={books} />
			)}
			<Button
				onPress={() => navigation.navigate('CreateBook')}
				containerStyle={styles.addButton}
				title="Add book"
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		justifyContent: 'flex-end',
		paddingBottom: wp(10),
		flex: 1,
		alignItems: 'center',
	},
	addButton: {
		position: 'absolute',
		bottom: wp(5),
		width: wp(80),
	},
});

export default AllBooksScreen;
