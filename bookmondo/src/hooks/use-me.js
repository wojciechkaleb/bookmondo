import React, {useMemo, useState} from 'react'
import auth from '@react-native-firebase/auth';

export const useMe = () => {
	const me = useMemo(() => auth().currentUser)

	return [me]
}