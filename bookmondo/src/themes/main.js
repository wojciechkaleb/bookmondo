import {Colors} from '@styles';
export default {
	colors: {
		primary: Colors.PRIMARY_BLUE,
		secondary: Colors.SECONDARY_PINK,
		white: Colors.WHITE,
		black: Colors.BLACK,
		grey0: Colors.GREY0,
		grey1: Colors.GREY1,
		grey2: Colors.GREY2,
		grey3: Colors.GREY3,
		grey4: Colors.GREY4,
		grey5: Colors.GREY5,
		greyOutline: Colors.grey0,
		success: Colors.SUCCESS,
		error: Colors.ERROR,
		warning: Colors.WARNING,
		divider: Colors.SECONDARY_PINK
	}
}