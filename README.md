To install and run application you can choose one of following:

1. Setting up environment using Android Studio (recommended)
2. Manually configuring environment

In both cases make sure the JDK you are installing is version 8, which works well with React Native.

## Installing Android Studio
To download you can use [this link](https://developer.android.com/studio). Make sure you also installed Android SDK and ADB. If you prefer installing by your package manager, then install following packages:

* `android-studio`
* `android-sdk`
* `android-sdk-platform-tools`

JDK is provided by Android SDK, make sure you install **v8** which works well with React Native.

#### Installing SDK packages
To install SDK package that go to `Tools -> SDK Manager` and install one of packages.

#### Creating AVD
To create AVD go to `Tools -> AVD Manager`.

## Manually configuring environment
If you don't want whole Android Studio IDE, you can just install following packages:


* `android-sdk`
* `android-sdk-platform-tools`
* `android-emulator`

JDK is provided by Android SDK, make sure you install **v8** which works well with React Native.

You still need to accept SDK package license:

1. Make yourself owner of `ANDROID_HOME`, as it will be also required to run application using ADB.

```sudo chown -R $(whoami) $ANDROID_HOME``` 

2. Accept all licences

``` yes | sdkmanager --licenses ```


### Emulator

#### Installing SDK packages
In order to create AVD, you need to install SDK package. Full list on possible packages you can find using `sdkmanager --list`.  To install one of them:

```cd $ANDROID_HOME/tools/bin```

```./sdkmanager "system-images;android-25;default;x86"```

[More information about `sdkmanager` here.](https://developer.android.com/studio/command-line/sdkmanager)

#### Creating AVD
To run app on emulator you need to create AVD, full list of devices you can find using `avdmanager list`. To install AVD use

```avdmanager create avd -n name -k "sdk_id" ```

[More about `avdmanager` here.](https://developer.android.com/studio/command-line/avdmanager)

So for example, to create AVD (Nexus 6P) named `patients` using the x86 system image for API level 25:

```avdmanager create avd --name patients --package 'system-images;android-25;default;x86' --device "Nexus 6P"```


Once you create AVD, you can start it using:

```emulator -avd patients```

## Running on Android device
If you want to use physical device for this, you have to enable `Developer Mode` and `USB Debugging`. Since Android 4.2 this option is hidden in settings. To enable `Developer Mode` you need to go to `Settings ->  About Phone`, and tap couple times on `Build Number` (or something similar). Then tap the Back button and you should see `Developer Options` where you can find `USB Debugging`.

#### Setting up ADB (device connected via USB / emulator).

To find your device use `adb devices`. To create port forwarding use:

```adb -s <device-name> reverse tcp:8081 tcp:8081```  


### Starting React server
To start React server use:

```yarn run start```


### Starting application on Android
Once you have ADB ready you can run it on physical device / emulator:
```yarn run android```


### Wireless debug
**NOTE:** You have to run application at least once using USB (to install app).

Once you have application installed and your React server is running:
1. Open application on device

2. Shake device to run developer menu

3. Go to `Settings -> Debug server host & port for device`

4. Get the IP Address of your Development Machine
	* To get internal IP you can use `ifconfig | grep -w inet | awk '{ print $2}'`
5. Enter `host:port` in Debug server menu (port is 8081 by default).

You might consider using [Flipper](https://fbflipper.com/) for deep logs.
